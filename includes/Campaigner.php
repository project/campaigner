<?php

/**
 * @file
 * Campaigner API integration.
 */

/**
 * Campaigner API Class.
 */
class Campaigner {

  private $client;
  private $authentication;
  private $contactData = array();

  /**
   * Default Constructor.
   *
   * @param string $wsdl
   *   URI of the WSDL file.
   * @param string $username
   *   Api user's username.
   * @param string $password
   *   Api user's password.
   */
  public function __construct($wsdl, $username, $password) {
    ini_set('default_socket_timeout', 60);

    $this->client = new SoapClient($wsdl, array(
      'exceptions' => FALSE,
      'compression' => SOAP_COMPRESSION_ACCEPT | SOAP_COMPRESSION_GZIP,
      'soap_version' => 'SOAP_1_1',
      'trace' => TRUE,
      'connection_timeout' => 300,
    ));

    $this->authentication = array(
      'Username' => $username,
      'Password' => $password,
    );
  }

  /**
   * Add contact data to be used in immediateUpload().
   *
   * @param array $groups
   *   Group Ids of where the contact will be added to.
   * @param string $email
   *   Email address (system field).
   * @param string $first_name
   *   First Name (system field).
   * @param string $last_name
   *   Last Name (system field).
   * @param string $phone_number
   *   Phone Number (system field).
   * @param array $custom_attributes
   *   Any custom field values. These values need to be in
   *   an array Ex:  array(
   *                  "IsNull" => FALSE,
   *                  "Id" => 5393483,
   *                  "Value" => "United States",
   *                 );.
   * @param bool $is_test_contact
   *   Is this contact a test contact.
   */
  public function addContactData(array $groups, $email, $first_name = '', $last_name = '', $phone_number = '', array $custom_attributes = array(), $is_test_contact = FALSE) {
    // Set all the custom fields.
    $attributes = array();
    foreach ($custom_attributes as $custom_attribute) {
      $attributes[] = array(
        "IsNull" => isset($custom_attribute['IsNull']) ? $custom_attribute['IsNull'] : FALSE,
        "Id" => isset($custom_attribute['Id']) ? $custom_attribute['Id'] : '',
        "_" => isset($custom_attribute['Value']) ? $custom_attribute['Value'] : '',
      );
    }

    $this->contactData[] = array(
      'IsTestContact' => $is_test_contact,
      'ContactKey' => array(
        'ContactId' => 0,
        'ContactUniqueIdentifier' => $email,
      ),
      'EmailAddress' => $email,
      'FirstName' => $first_name,
      'LastName' => $last_name,
      'PhoneNumber' => $phone_number,
      'CustomAttributes' => $attributes,
      'AddToGroup' => $groups,
    );
  }

  /**
   * Call the immediateUpload api method.
   *
   * @param bool $update_existing_contacts
   *   If you want to update an existing value.
   * @param bool $trigger_workflow
   *   Trigger workflow.
   *
   * @return mixed
   *   Api response.
   */
  public function immediateUpload($update_existing_contacts = TRUE, $trigger_workflow = FALSE) {
    $response = $this->client->ImmediateUpload(array(
      'authentication' => $this->authentication,
      'UpdateExistingContacts' => $update_existing_contacts,
      'TriggerWorkflow' => $trigger_workflow,
      'contacts' => array(
        'ContactData' => $this->contactData,
      ),
    ));

    return $response;
  }

  /**
   * Get last response.
   *
   * @return string
   *   Last response.
   */
  public function getLastResponse() {
    return $this->client->__getLastResponse();
  }

  /**
   * Create/Update campaign in Campaigner.
   *
   * If you want to update campaign include the campaign id.
   *
   * @param array $campaignData
   *   Array containing campaign data.
   *
   * @code
   *   $campaignData = array(
   *     "id" => 'The unique identifier for the campaign you want to create or update. (can be null)',
   *     "CampaignName" => 'The name of the campaign goes here.'
   *     "CampaignSubject" => 'Subject goes here.',
   *     "CampaignFormat" => 'HTML',
   *     "CampaignType" => 'OneOff',
   *     "HtmlContent" => "<html><head></head><body><h1>Test HTML</h1></body></html>",
   *     "TxtContent" => 'This is Text For the Campaign',
   *     "FromName" => 'Campaigner Test',
   *     "FromEmailId" => 1343, // Email-Id which appears as From Email-Id when Campaign is Sent.
   *     "ReplyEmailId" => 3343, // Email-Id to which repelies for this campaign are sent.
   *     "TrackReplies" => true,
   *     "MailingAddressSettings" => array(
   *       "IncludeMailingAddress" => true,
   *       "MailingAddress" => 'Enter Address Here',
   *     ),
   *   );
   * @endcode
   */
  public function createUpdateCampaign(array $campaignData) {
    $campaignId = NULL;

    for ($attempt = 1; $attempt <= 5; $attempt++) {
      try {
        $response = $this->client->CreateUpdateCampaign(array('authentication' => $this->authentication, 'campaignData' => $campaignData));

        if (is_soap_fault($response)) {
          watchdog('campaigner', 'API CreateUpdateCampaign: CreateUpdateCampaign Failed with SOAP Fault. Please Check Your Request, Endpoint, Connection Settings Etc.', array(), WATCHDOG_ERROR);
        }
        elseif (isset($response)) {
          $dom = new DOMDocument();
          $dom->loadXML($this->client->__getLastResponse());
          $errorFlag = $dom->getElementsByTagName('ErrorFlag')->item(0)->nodeValue;
          $returnMessage = $dom->getElementsByTagName('ReturnMessage')->item(0)->nodeValue;
          $returnCode = $dom->getElementsByTagName('ReturnCode')->item(0)->nodeValue;

          if ($errorFlag == 'false' && $returnCode == 'M_1.1.1_SUCCESS') {
            $campaignId = $dom->getElementsByTagName('CampaignId')->item(0)->nodeValue;

            if ($campaignId == NULL) {
              $message = 'API CreateUpdateCampaign: CreateUpdateCampaign Returned Success Without Any Campaign Id. Check the CreateUpdateCampaign Request.';
              watchdog('campaigner', $message, array(), WATCHDOG_ERROR);
              throw new Exception($message);
            }

            return $campaignId;
          }
          elseif (strpos($returnCode, 'U_') !== FALSE) {
            $message = "API CreateUpdateCampaign: CreateUpdateCampaign Returned User_Error. Please Correct the Request. Return Code = {$returnCode} And Return Message = {$returnMessage}";
            watchdog('campaigner', $message, array(), WATCHDOG_ERROR);
            throw new Exception($message);
          }
          elseif (strpos($returnCode, 'E_') !== FALSE) {
            $message = "API CreateUpdateCampaign: CreateUpdateCampaign Returned Internal Error. Please Contact Campaigner Support. Return Code = {$returnCode} And Return Message = {$returnMessage}";
            watchdog('campaigner', $message, array(), WATCHDOG_ERROR);
            throw new Exception($message);
          }
          else {
            $message = "API CreateUpdateCampaign: CreateUpdateCampaign Returned Unknown ReturnCode. {$returnCode} Please Contact Campaigner Support";
            watchdog('campaigner', $message, array(), WATCHDOG_ERROR);
            throw new Exception($message);
          }
        }
        else {
          $message = "API: CreateUpdateCampaign API Response is NULL Even After 5 Retries. Contact Campaigner Support.";
          watchdog('campaigner', $message, array(), WATCHDOG_ERROR);
          throw new Exception($message);
        }
      }
      catch (Exception $e) {
        if ($attempt == 5) {
          watchdog('campaigner', 'API CreateUpdateCampaign: %message', array(
            '%message' => $e->getMessage(),
          ), WATCHDOG_ERROR);
          throw $e;
        }
        else {
          watchdog('campaigner', "API CreateUpdateCampaign: ScheduleCampaign failed. Retrying Now. Retry Attempt = {$attempt}", array(), WATCHDOG_WARNING);
          sleep(5);
        }
      }
    }

    return $campaignId;
  }

  /**
   * Set recipients to campaigns.
   *
   * @param int $campaignId
   *   Campaign ID.
   * @param array $campaignRecipients
   *   Array of redipient ids.
   * @param bool $sendToContacts
   *   Set to TRUE if you want to send to all contacts.
   *
   * @return bool
   *   Will return TRUE if successful.
   */
  public function setCampaignRecipients($campaignId, array $campaignRecipients = array(), $sendToContacts = FALSE) {
    $campaignRecipients = array(
      'sendToAllContacts' => $sendToContacts,
      'contactGroupIds' => $campaignRecipients,
    );

    for ($attempt = 1; $attempt <= 5; $attempt++) {
      try {
        $response = $this->client->SetCampaignRecipients(array(
          'authentication' => $this->authentication,
          'campaignId' => $campaignId,
          'campaignRecipients' => $campaignRecipients,
        ));

        if (is_soap_fault($response)) {
          watchdog('campaigner', 'API SetCampaignRecipients: Failed with SOAP Fault. Please Check Your Request, Endpoint, Connection Settings Etc.', array(), WATCHDOG_ERROR);
        }
        elseif (isset($response)) {
          $dom = new DOMDocument();
          $dom->loadXML($this->client->__getLastResponse());
          $errorFlag     = $dom->getElementsByTagName('ErrorFlag')->item(0)->nodeValue;
          $returnMessage = $dom->getElementsByTagName('ReturnMessage')->item(0)->nodeValue;
          $returnCode    = $dom->getElementsByTagName('ReturnCode')->item(0)->nodeValue;
          if ($errorFlag == 'false' && $returnCode == 'M_1.1.1_SUCCESS') {
            return TRUE;
          }
        }
      }
      catch (Exception $e) {
        if ($attempt == 5) {
          watchdog('campaigner', 'API SetCampaignRecipients: %message', array(
            '%message' => $e->getMessage(),
          ), WATCHDOG_ERROR);
          throw $e;
        }
        else {
          sleep(5);
        }
      }
    }

    return FALSE;
  }

  /**
   * Schedule campaign.
   *
   * @param int $campaignId
   *   Campaign id.
   * @param bool $sendNow
   *   Send the campaign now.
   *
   * @return bool
   *   Will return TRUE if scheduled.
   */
  public function scheduleCampaign($campaignId, $sendNow) {
    for ($attempt = 1; $attempt <= 5; $attempt++) {
      try {
        $response = $this->client->ScheduleCampaign(array(
          'authentication' => $this->authentication,
          'campaignId' => $campaignId,
          'sendNow' => $sendNow,
        ));

        if (is_soap_fault($response)) {
          throw new Exception("API: ScheduleCampaign Failed with SOAP Fault. Please Check Your Request, Endpoint, Connection Settings Etc.");
        }

        elseif (isset($response)) {
          $dom = new DOMDocument();
          $dom->loadXML($this->client->__getLastResponse());
          $errorFlag     = $dom->getElementsByTagName('ErrorFlag')->item(0)->nodeValue;
          $returnMessage = $dom->getElementsByTagName('ReturnMessage')->item(0)->nodeValue;
          $returnCode    = $dom->getElementsByTagName('ReturnCode')->item(0)->nodeValue;
          if ($errorFlag == 'false' && $returnCode == 'M_1.1.1_SUCCESS') {
            return TRUE;
          }
        }
      }
      catch (Exception $e) {
        if ($attempt == 5) {
          watchdog('campaigner', 'API ScheduleCampaign: %message', array(
            '%message' => $e->getMessage(),
          ), WATCHDOG_ERROR);
          throw $e;
        }
        else {
          sleep(5);
        }
      }
    }

    return FALSE;
  }

  /**
   * Send Test campaign.
   *
   * @param int $campaign_id
   *   Campaign Id.
   * @param int $contact_id
   *   Contact id.
   * @param array $emails
   *   Array of Email Addresses.
   *
   * @return bool
   *   Passed or failed.
   */
  public function sendTestCampaign($campaign_id, $contact_id, array $emails = array()) {
    $response = $this->client->SendTestCampaign(array(
      'authentication' => $this->authentication,
      'campaignId' => $campaign_id,
      'contactKeyForTest' => array(
        'ContactId' => $contact_id,
        'ContactUniqueIdentifier' => '',
      ),
      'emails' => $emails,
    ));

    if (is_soap_fault($response)) {
      throw new Exception("API: ScheduleCampaign Failed with SOAP Fault. Please Check Your Request, Endpoint, Connection Settings Etc.");
    }
    elseif (isset($response)) {
      $dom = new DOMDocument();
      $dom->loadXML($this->client->__getLastResponse());
      $errorFlag     = $dom->getElementsByTagName('ErrorFlag')->item(0)->nodeValue;
      $returnMessage = $dom->getElementsByTagName('ReturnMessage')->item(0)->nodeValue;
      $returnCode    = $dom->getElementsByTagName('ReturnCode')->item(0)->nodeValue;
      if ($errorFlag == 'false' && $returnCode == 'M_1.1.1_SUCCESS') {
        return TRUE;
      }
    }

    return FALSE;
  }

  /**
   * Lists from emails.
   *
   * Lists all the validated (can be used as from/reply-to) and pending (can not
   * be used) email addresses associated with the account.
   *
   * @return array
   *   Array of email addresses.
   */
  public function listFromEmails($validated = TRUE) {
    $emails = array();

    $response = $this->client->ListFromEmails(array(
      'authentication' => $this->authentication,
    ));

    if (isset($response->ListFromEmailsResult->FromEmailDescription[0])) {
      foreach ($response->ListFromEmailsResult->FromEmailDescription as $key => $emailObject) {
        if ($validated && $emailObject->FromEmailStatus == 'Validated') {
          $emails[$emailObject->Id] = $emailObject->EmailAddress;
        }
      }
    }

    return $emails;
  }

}
