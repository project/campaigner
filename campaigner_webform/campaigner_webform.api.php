<?php

/**
 * @file
 * Hooks provided by the Campaigner Webfomr module.
 */

/**
 * All other modules to modify the recipient data.
 *
 * @param array &$recipient
 *   The recipent campaigner array.
 * @param array $form
 *   Form object.
 * @param array $form_state
 *   Form state object.
 * @param object $node
 *   Node object.
 */
function hook_campaigner_webform_recipient_alter(array &$recipient, array &$form, array &$form_state, $node) {
  if ($form['#form_id'] == 'my_form_id') {
    $values = campaigner_webform_form_collapse($form, $form_state);

    // If not checked don't add to campaigner.
    if (!$values[2]) {
      $recipient = NULL;
    }
  }
}
