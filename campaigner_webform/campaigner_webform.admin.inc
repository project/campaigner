<?php

/**
 * @file
 * Admin forms.
 */

/**
 * Form constructor for the campaigner management form.
 */
function campaigner_webform_components_form($form, $form_state, $webform_node) {
  $form = array();

  $form['#tree'] = TRUE;
  $form['#node'] = $webform_node;
  $nid = $webform_node->nid;
  $record = campaigner_webform_load($nid);
  if ($record) {
    campaigner_get_array_from_textarea($record->group_ids);
  }

  $form['#record'] = $record;

  if (!isset($record->is_active)) {
    $record = new stdClass();
    $record->is_active = TRUE;
    $record->group_ids = "";
  }
  $form['details'] = array(
    '#type' => 'fieldset',
    '#title' => t('Campaigner Settings'),
  );
  $form['details']['is_active'] = array(
    '#title' => 'Is active',
    '#type' => 'checkbox',
    '#default_value' => $record->is_active,
  );
  $form['details']['group_ids'] = array(
    '#type' => 'textarea',
    '#title' => t('Campaigner Group Ids'),
    '#default_value' => !empty($record->group_ids) ? $record->group_ids : '',
    '#description' => 'Enter one group id per line',
    '#states' => array(
      'visible' => array(
        '#edit-details-is-active' => array('checked' => TRUE),
      ),
    ),
  );
  $form['components'] = array(
    '#tree' => TRUE,
  );
  foreach ($webform_node->webform['components'] as $k => $component) {
    $form['components'][$k] = array(
      '#component' => $component,
      'key' => array(
        '#title' => 'Field name',
        '#type' => 'textfield',
        '#size' => 25,
        '#default_value' => isset($record->data[$component['form_key']]['key']) ? $record->data[$component['form_key']]['key'] : '',
      ),
    );
  }
  $form['actions']['save'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  $form['actions']['delete'] = array(
    '#type' => 'submit',
    '#value' => t('Reset'),
    '#submit' => array('campaigner_webform_components_form_delete_submit'),
  );

  return $form;
}

/**
 * Theme callback for campaigner_webform_components_form().
 */
function theme_campaigner_webform_components_form($form) {
  $form = $form['form'];
  $rows = array();
  $output = '';

  $header = array(t('Name'), t('Type'), t('Campaigner key'));

  foreach (element_children($form['components']) as $k) {
    $row = array();
    // Name.
    $row[] = $form['#node']->webform['components'][$k]['name'];
    // Type.
    $row[] = $form['#node']->webform['components'][$k]['type'];
    // Campaigner key.
    unset($form['components'][$k]['key']['#title']);
    $row[] = drupal_render($form['components'][$k]['key']);
    $rows[] = $row;
  }

  $output .= drupal_render($form['details']);
  $output .= theme('table', array('header' => $header, 'rows' => $rows));
  $output .= '<p>Use the <b>EmailAddress</b> for email, <b>FirstName</b>
              for first name, <b>LastName</b> for last name, and <b>PhoneNumber</b> for phone number.</p>';
  $output .= drupal_render_children($form);

  return $output;
}

/**
 * Form validator for the campaigner webform management form.
 */
function campaigner_webform_components_form_validate($form, &$form_state) {
  $values = $form_state['values'];
  if (!empty($values['details']['is_active']) && empty($values['details']['group_ids'])) {
    form_error($form['details']['group_ids'], t('Campaigner Group ID(s) are required'));
  }
}

/**
 * Form submission handler for campaigner_webform_components_form().
 */
function campaigner_webform_components_form_submit($form, $form_state) {
  $node = $form['#node'];

  $record = $form['#record'];
  if ($record) {
    $update = array('nid');
  }
  else {
    $record = new stdClass();
    $update = array();
  }

  $data = array();
  foreach (element_children($form['components']) as $k) {
    if ($form_state['values']['components'][$k]['key']) {
      $component = $form['components'][$k]['#component'];
      $data[$component['form_key']]['key'] = $form_state['values']['components'][$k]['key'];
    }
  }
  $record->nid = $node->nid;
  $record->is_active = (bool) $form_state['values']['details']['is_active'];
  $record->group_ids = trim($form_state['values']['details']['group_ids']);
  $record->data = $data;
  drupal_write_record('campaigner_webform', $record, $update);
}

/**
 * Form submission handler for campaigner_webform_components_form().
 */
function campaigner_webform_components_form_delete_submit($form, &$form_state) {
  $node = $form['#node'];
  db_delete('campaigner_webform')
    ->condition('nid', $node->nid)
    ->execute();

  drupal_set_message(t('Campaigner settings for this webform deleted.'), 'status');
}
