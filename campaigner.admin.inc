<?php

/**
 * @file
 * Admin file for the campaigner module.
 */

/**
 * Campaigner admin form.
 */
function campaigner_admin() {
  $form = array();

  $form['campaigner_api']['api'] = array(
    '#markup' => '<p>Add API configuration here. These settings are required
      for Webform integration.</p>',
  );

  $form['campaigner_api']['campaigner_username'] = array(
    '#type' => 'textfield',
    '#title' => t('Campaigner username'),
    '#default_value' => variable_get('campaigner_username', ''),
    '#size' => 60,
    '#maxlength' => 255,
    '#description' => 'The username to the user that has access to Campaigner
                       API.',
    '#required' => TRUE,
  );

  $form['campaigner_api']['campaigner_password'] = array(
    '#type' => 'password',
    '#title' => t('Campaigner password'),
    '#default_value' => variable_get('campaigner_password', ''),
    '#size' => 60,
    '#maxlength' => 255,
    '#description' => 'The password to the user that has access to Campaigner
                       API.',
  );

  $form['campaigner_api']['campaigner_user_wsdl'] = array(
    '#type' => 'textfield',
    '#title' => t('WSDL - User Management'),
    '#default_value' => variable_get('campaigner_user_wsdl', ''),
    '#size' => 255,
    '#maxlength' => 255,
    '#description' => 'The WSDL url to access API for user functions.',
  );

  $form['campaigner_api']['campaigner_campaign_wsdl'] = array(
    '#type' => 'textfield',
    '#title' => t('WSDL - Campaign Management'),
    '#default_value' => variable_get('campaigner_campaign_wsdl', ''),
    '#size' => 255,
    '#maxlength' => 255,
    '#description' => 'The WSDL url to access API for campaign functions.',
  );

  $form = system_settings_form($form);

  $form['#submit'] = array('campaigner_encrypt_password', 'system_settings_form_submit');

  return $form;
}

/**
 * Encrypt password (if necessary).
 */
function campaigner_encrypt_password($form, &$form_state) {
  if (!empty($form_state['values']['campaigner_password'])) {
    $password = $form_state['values']['campaigner_password'];
    $form_state['values']['campaigner_password'] = campaigner_set_password($password);
  }
  else {
    $form_state['values']['campaigner_password'] = campaigner_get_password(FALSE);
  }
}
