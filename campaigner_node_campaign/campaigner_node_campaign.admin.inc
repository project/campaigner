<?php

/**
 * @file
 * Admin forms for Campaigner Node Campaign module.
 */

/**
 * Campaigner Node Campaign settings form.
 */
function campaigner_node_campaign_settings_form($form, &$form_state) {
  $campaigner = campaigner_node_campaign_get_campaigner_object();

  $from_emails = $campaigner->listFromEmails();

  $form['campaigner_node_campaign_node_types'] = array(
    '#type' => 'checkboxes',
    '#options' => node_type_get_names(),
    '#title' => t('Node Types'),
    '#default_value' => variable_get('campaigner_node_campaign_node_types', array()),
    '#required' => TRUE,
  );

  $form['campaigner_node_campaign_list'] = array(
    '#type' => 'textfield',
    '#title' => t('Campaigner List ID(s)'),
    '#default_value' => variable_get('campaigner_node_campaign_list', ''),
    '#description' => t('The list that your campaign will be sent to. You may comma seperate your lists.'),
    '#required' => TRUE,
  );

  $form['campaigner_node_campaign_from'] = array(
    '#type' => 'select',
    '#title' => t('Campaigner From Email ID'),
    '#default_value' => variable_get('campaigner_node_campaign_from', ''),
    '#description' => t('The email id of the user that the campaign will be from.'),
    '#options' => $from_emails,
    '#required' => TRUE,
  );

  $form['campaigner_node_campaign_from_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Campaigner From Name'),
    '#default_value' => variable_get('campaigner_node_campaign_from_name', ''),
    '#description' => t('The sender’s name displayed as the From email address in the email header of the email campaign.'),
    '#required' => TRUE,
  );

  $form['campaigner_node_campaign_template'] = array(
    '#type' => 'textarea',
    '#title' => t('HTML Template'),
    '#default_value' => variable_get('campaigner_node_campaign_template', ''),
    '#description' => 'Paste full HTML newsletter',
    '#rows' => 30,
    '#required' => TRUE,
  );

  // Display the list of available placeholders if token module is installed.
  if (module_exists('token')) {
    $form['token_help'] = array(
      '#theme' => 'token_tree',
      '#token_types' => array('node'),
      '#dialog' => TRUE,
    );
  }

  $form = system_settings_form($form);

  return $form;
}

/**
 * Create campaign form.
 */
function campaigner_node_campaign_campaign_form($form, &$form_state, $value) {
  $form = array();
  $form = system_settings_form($form);

  $node = node_load($value);
  $campaign = campaigner_node_campaign_get_campaign($value);

  $form['nid'] = array(
    '#type' => 'hidden',
    '#value' => $value,
  );

  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Campaign Name'),
    '#description' => t('The name of the campaign.'),
    '#default_value' => (isset($campaign['name']) && $campaign['name']) ? $campaign['name'] : $node->title,
  );

  $form['subject'] = array(
    '#type' => 'textfield',
    '#title' => t('Campaign Subject'),
    '#description' => t('The text for the subject line of the campaign.'),
    '#default_value' => (isset($campaign['subject']) && $campaign['subject']) ? $campaign['subject'] : $node->title,
  );

  campaigner_node_campaign_add_preview_iframe($form, $node);

  $form['actions']['submit']['#value'] = 'Create/Update Campaign';
  $form['#submit'] = array('campaigner_node_campaign_campaign_form_submit');

  return $form;
}

/**
 * Submit handler for campaigner_node_campaign_campaign_form().
 */
function campaigner_node_campaign_campaign_form_submit($form, &$form_state) {
  $values = $form_state['values'];
  $campaign = campaigner_node_campaign_get_campaign($values['nid']);
  $node = node_load($values['nid']);
  $html_content = campaigner_node_campaign_get_html_content($node);

  if ($campaign) {
    // If subject or name changed update the campaign in Campaigner.
    if ($campaign['name'] != $values['name'] ||
        $campaign['subject'] != $values['subject'] ||
        $campaign['html_content'] != $html_content) {
      campaigner_node_campaign_update_campaign($values['nid'], $values['name'], $values['subject'], $html_content);
    }
  }
  else {
    campaigner_node_campaign_create_campaign($values['nid'], $values['name'], $values['subject'], $html_content);
  }

  drupal_goto('admin/config/services/campaigner/node/' . $values['nid'] . '/send');
}

/**
 * Send to campaigner.
 */
function campaigner_node_campaign_send_form($form, &$form_state, $value) {
  $form = array();
  $form = system_settings_form($form);

  $node = node_load($value);
  campaigner_node_campaign_add_preview_iframe($form, $node, FALSE);

  $form['nid'] = array(
    '#type' => 'hidden',
    '#value' => $value,
  );

  $form['#submit'] = array('campaigner_node_campaign_send_form_submit');

  return confirm_form($form,
                    'Are you sure you want to send this campaign?',
                    'admin/config/services/campaigner/node/' . $value . '/test', '',
                    t('Send Campaign'), t('Cancel'));
}

/**
 * Submit hanlder for campaigner_node_campaign_send_form().
 */
function campaigner_node_campaign_send_form_submit($form, &$form_state) {
  $sent = campaigner_node_campaign_send_campaign($form_state['values']['nid']);
  if ($sent) {
    drupal_set_message(t('Your campaign has been scheduled to be sent out.'));
    drupal_goto('node/' . $form_state['values']['nid']);
  }
  else {
    drupal_set_message(t('Your campaign failed to send or it was already sent. Please try again if it was not sent.'), 'error');
    drupal_goto('admin/config/services/campaigner/node/' . $form['nid']['#value'] . '/send');
  }
}

/**
 * Adds preview iframe to form.
 *
 * @param int &$form
 *   Form array.
 * @param object $node
 *   Node object.
 * @param bool $show_description
 *   Show the preview description.
 */
function campaigner_node_campaign_add_preview_iframe(&$form, $node, $show_description = TRUE) {
  $form['preview'] = array(
    '#markup' => '<h2>Preview</h2><iframe sandbox="allow-scripts allow-same-origin" id="markupframe" src="about:blank" width="100%" height="500px"></iframe>',
  );

  if ($show_description) {
    $form['preview_description'] = array(
      '#markup' => '<p>If you need to make changes to the newsletter, please go back to your node and make any changes to the body or the body summary.',
    );
  }

  // Add the html with javascript (We may be able to use srcdoc in the future
  // but currently it isn't supported on all browsers).
  drupal_add_js(array(
    'campaigner_node_campaign' => array(
      'html' => campaigner_node_campaign_get_html_content($node),
    ),
  ), 'setting');

  drupal_add_js('jQuery(document).ready(function () {
    setTimeout( function() {
      var frame = document.getElementById("markupframe");
      var fdoc = frame.contentDocument;
      fdoc.write(Drupal.settings.campaigner_node_campaign.html);
    }, 1);
  });',
  array(
    'type' => 'inline',
    'scope' => 'footer',
  ));
}
