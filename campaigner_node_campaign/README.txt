CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------
The Campaigner Node Campaign Module integrates with Campaigner (http://www.campaigner.com).
It allows you to define a HTML template (allow to use tokens) and give the
ability for admins to create/send out Campaigns based on node information using
tokens in your template.


REQUIREMENTS
------------
The campaigner webform module requires the following modules:
  * Campaigner


INSTALLATION
------------
Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.


CONFIGURATION
-------------
Configure user permissions in  Administration » Configuration » Web services »
Campaigner settings » Campaigner Node Settings


MAINTAINERS
-----------
Current maintainers:
 * Albert Jankowski (albertski) - https://www.drupal.org/u/albertski
