<?php

/**
 * @file
 * Module file for Campaigner Node Campaign module.
 */

/**
 * Implements hook_menu().
 */
function campaigner_node_campaign_menu() {
  $items['admin/config/services/campaigner/node'] = array(
    'title' => 'Campaigner Node Settings',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('campaigner_node_campaign_settings_form', 1),
    'access arguments' => array('administer campaigner node campaign'),
    'file' => 'campaigner_node_campaign.admin.inc',
    'weight' => 1,
    'type' => MENU_LOCAL_TASK,
  );

  $items['admin/config/services/campaigner/node/%/campaign'] = array(
    'title' => 'Create/Update Campaign',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('campaigner_node_campaign_campaign_form', 5),
    'access arguments' => array('administer campaigner node campaign'),
    'file' => 'campaigner_node_campaign.admin.inc',
    'weight' => 1,
    'type' => MENU_CALLBACK,
  );

  $items['admin/config/services/campaigner/node/%/send'] = array(
    'title' => 'Campaigner Send',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('campaigner_node_campaign_send_form', 5),
    'access arguments' => array('administer campaigner node campaign'),
    'file' => 'campaigner_node_campaign.admin.inc',
    'weight' => 1,
    'type' => MENU_CALLBACK,
  );

  return $items;
}

/**
 * Implements hook_permission().
 */
function campaigner_node_campaign_permission() {
  return array(
    'administer campaigner node campaign' => array(
      'title' => t('Administer Campaigner Node Campaign'),
    ),
  );
}

/**
 * Implements hook_form_alter().
 */
function campaigner_node_campaign_form_node_form_alter(&$form, &$form_state) {

  $selected_node_types = variable_get('campaigner_node_campaign_node_types', array());
  $node_types = array();
  foreach ($selected_node_types as $value) {
    if ($value) {
      $node_types[] = $value;
    }
  }

  $node = $form['#node'];

  if (!isset($node->nid)|| !in_array($node->type, $node_types)) {
    return NULL;
  }

  $form['campaigner_node_campaign'] = array(
    '#type' => 'fieldset',
    '#tree' => TRUE,
    '#title' => t('Campaigner'),
    '#collapsible' => TRUE,
    '#access' => user_access('administer campaigner node campaign'),
    '#group' => 'additional_settings',
  );

  $campaigner = campaigner_node_campaign_get_campaign($node->nid);

  if ($campaigner && $campaigner['sent']) {
    $form['campaigner_node_campaign']['previous_sent'] = array(
      '#type' => 'item',
      '#markup' => '<p>' . t('Campaign already sent on: @date', array('@date' => date('l F, jS Y h:i:s A', $campaigner['sent']))) . '<p>',
    );
  }
  else {
    $form['campaigner_node_campaign']['send_campaign'] = array(
      '#type' => 'item',
      '#markup' => '<a href="/admin/config/services/campaigner/node/' . $node->nid . '/campaign" class="button">Send Campaign</a>',
    );
  }
}

/**
 * Create campaign in Campaigner.
 *
 * @param int $nid
 *   Node id.
 *
 * @return int
 *   Campaign id.
 */
function campaigner_node_campaign_create_campaign($nid, $name, $subject, $html_content) {
  // Check if there is a campaign already for this node.
  $campaign_id = campaigner_node_campaign_get_campaign_id($nid);

  if (!$campaign_id) {
    $campaign_data = array(
      'CampaignName' => $name,
      'CampaignSubject' => $subject,
      'CampaignFormat' => 'HTML',
      'CampaignType' => 'OneOff',
      'HtmlContent' => $html_content,
      'FromEmailId' => variable_get('campaigner_node_campaign_from'),
      'FromName' => variable_get('campaigner_node_campaign_from_name'),
      'ReplyEmailId' => variable_get('campaigner_node_campaign_from'),
      'TrackReplies' => TRUE,
      'MailingAddressSettings' => array(
        'IncludeMailingAddress' => FALSE,
      ),
    );

    $campaigner = campaigner_node_campaign_get_campaigner_object();

    // Create the campaign.
    $campaign_id = $campaigner->createUpdateCampaign($campaign_data);

    // Add list to campaign.
    $campaigner->setCampaignRecipients($campaign_id, explode(',', variable_get('campaigner_node_campaign_list', array())));

    db_insert('campaginer_node_campaign')
      ->fields(array(
        'nid' => $nid,
        'campaign_id' => $campaign_id,
        'name' => $name,
        'subject' => $subject,
        'html_content' => $html_content,
      ))
      ->execute();
  }

  return $campaign_id;
}

/**
 * Update campaign in Campaigner.
 *
 * @param int $nid
 *   Node id.
 * @param string $name
 *   Name string.
 * @param string $subject
 *   Subject string.
 */
function campaigner_node_campaign_update_campaign($nid, $name, $subject) {
  $campaign_id = campaigner_node_campaign_get_campaign_id($nid);

  $node = node_load($nid);
  $html_content = campaigner_node_campaign_get_html_content($node);

  $campaign_data = array(
    'Id' => $campaign_id,
    'CampaignName' => $name,
    'CampaignSubject' => $subject,
    'CampaignFormat' => 'HTML',
    'CampaignType' => 'OneOff',
    'HtmlContent' => $html_content,
    'FromEmailId' => variable_get('campaigner_node_campaign_from'),
    'FromName' => variable_get('campaigner_node_campaign_from_name'),
    'ReplyEmailId' => variable_get('campaigner_node_campaign_from'),
    'TrackReplies' => TRUE,
    'MailingAddressSettings' => array(
      'IncludeMailingAddress' => FALSE,
    ),
  );

  $campaigner = campaigner_node_campaign_get_campaigner_object();

  $campaigner->createUpdateCampaign($campaign_data);

  // Add list to campaign.
  $campaigner->setCampaignRecipients($campaign_id, explode(',', variable_get('campaigner_node_campaign_list', array())));

  db_update('campaginer_node_campaign')
    ->fields(array(
      'name' => $name,
      'subject' => $subject,
      'html_content' => $html_content,
    ))
    ->condition('nid', $nid)
    ->execute();
}

/**
 * Get campaign id.
 *
 * @param int $nid
 *   Node id.
 *
 * @return object
 *    campaginer_node_campaign object.
 */
function campaigner_node_campaign_get_campaign($nid) {
  $campaign = db_select('campaginer_node_campaign', 'c')
    ->fields('c')
    ->condition('nid', $nid)
    ->execute()
    ->fetchAssoc();

  return $campaign;
}

/**
 * Get campaign id.
 *
 * @param int $nid
 *   Node id.
 *
 * @return object
 *   Campaginer_node_campaign object.
 */
function campaigner_node_campaign_get_campaign_id($nid) {
  $campaign_id = db_select('campaginer_node_campaign', 'c')
    ->fields('c', array('campaign_id'))
    ->condition('nid', $nid)
    ->execute()
    ->fetchField();

  return $campaign_id;
}

/**
 * Send campaign.
 *
 * @param int $nid
 *   Node id.
 *
 * @return bool
 *   If campain sent.
 */
function campaigner_node_campaign_send_campaign($nid) {
  $campaigner = campaigner_node_campaign_get_campaigner_object();

  $campaign_id = campaigner_node_campaign_get_campaign_id($nid);

  $responseSent = $campaigner->scheduleCampaign($campaign_id, TRUE);

  if ($responseSent) {
    db_update('campaginer_node_campaign')
      ->fields(array(
        'sent' => REQUEST_TIME,
      ))
      ->condition('nid', $nid)
      ->execute();

    return TRUE;
  }
  else {
    return FALSE;
  }
}

/**
 * Get HTML content with replaced tokens.
 *
 * @param object $node
 *   Node object.
 *
 * @return string
 *   HTML template with replaced tokens for that node.
 */
function campaigner_node_campaign_get_html_content($node) {
  return token_replace(variable_get('campaigner_node_campaign_template', ''), array('node' => $node));
}

/**
 * Get Campaigner object.
 *
 * @return object
 *   Campaigner object.
 */
function campaigner_node_campaign_get_campaigner_object() {
  // Include Campaigner API Class.
  module_load_include('php', 'campaigner', 'includes/Campaigner');

  $campaigner = new Campaigner(
    variable_get('campaigner_campaign_wsdl', ''),
    variable_get('campaigner_username', ''),
    campaigner_get_password()
  );

  return $campaigner;
}
