<?php

/**
 * @file
 * Integrates Drupal to Campaigner.
 */

/**
 * Implements hook_menu().
 */
function campaigner_menu() {
  $items['admin/config/services/campaigner'] = array(
    'title' => 'Campaigner settings',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('campaigner_admin'),
    'access arguments' => array('administer campaigner settings'),
    'type' => MENU_NORMAL_ITEM,
    'file' => 'campaigner.admin.inc',
    'description' => 'Manage Campaigner settings.',
  );

  return $items;
}

/**
 * Implements hook_permission().
 */
function campaigner_permission() {
  return array(
    'administer campaigner settings' => array(
      'title' => t('Administer Campaigner settings'),
    ),
  );
}

/**
 * Campaigner get password.
 *
 * @param bool $decrypt
 *   Decrypt password returned if TRUE.
 *
 * @return string
 *   Password string.
 */
function campaigner_get_password($decrypt = TRUE) {
  $password = variable_get('campaigner_password', '');

  // If mcrypt is being used we need to decrypt the password.
  if ($password && function_exists('mcrypt_encrypt') && $decrypt) {
    $key = campiagner_pad_key(drupal_substr(drupal_get_hash_salt(), 0, 32));
    $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
    $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
    $ciphertext_dec = base64_decode($password);
    $plaintext_dec = mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $key, $ciphertext_dec, MCRYPT_MODE_ECB, $iv);

    return rtrim($plaintext_dec, "\0");
  }
  elseif ($password) {
    return $password;
  }
  else {
    return '';
  }
}

/**
 * Campaigner Set Password.
 *
 * @param string $password
 *   Password string.
 *
 * @return string
 *   Password.
 */
function campaigner_set_password($password = '') {
  // If mcrypt is available we will encrypt the password in the database.
  if ($password && function_exists('mcrypt_encrypt')) {
    $key = campiagner_pad_key(drupal_substr(drupal_get_hash_salt(), 0, 32));
    $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
    $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
    $ciphertext = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $key, $password, MCRYPT_MODE_ECB, $iv);
    $ciphertext_base64 = base64_encode($ciphertext);

    // Store encrypted password.
    return $ciphertext_base64;
  }
  // Otherwise it will be in plain text.
  elseif ($password) {
    return $password;
  }
}

/**
 * Pad the key to fix mcrypt issue in PHP 5.6.
 *
 * @param string $key
 *   Key.
 *
 * @return string
 *   Padded key.
 */
function campiagner_pad_key($key) {
  // Key is too large.
  if (strlen($key) > 32) {
    return FALSE;
  }

  // Set sizes.
  $sizes = array(16, 24, 32);

  // Loop through sizes and pad key.
  foreach ($sizes as $s) {
    while (strlen($key) < $s) {
      $key = $key . "\0";
    }

    if (strlen($key) == $s) {
      break; /* Finish if the key matches a size. */
    }
  }

  return $key;
}
