CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers
 * Troubleshooting


INTRODUCTION
------------
The Campaigner Module integrates with Campaigner (http://www.campaigner.com).
The Campaigner Webform submodule adds Webform integration to allow you to add
users to Campaigner's database.


REQUIREMENTS
------------
The campaigner webform module requires the following modules:
  * Campaigner
  * Webform (https://www.drupal.org/project/webform)


INSTALLATION
------------
Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.


CONFIGURATION
-------------
Configure user permissions in  Administration » Configuration » Web services »
Campaigner settings


TROUBLESHOOTING
---------------
I submit a form that is hooked up to Campaigner but I don't see the user added
in my Campaigner database.
  - Make sure your credentials are set in the Campaigner Settings
  - If the API call fails it saves the response in watchdog. Check your system
    logs for more information.


MAINTAINERS
-----------
Current maintainers:
 * Albert Jankowski (albertski) - https://www.drupal.org/u/albertski
